#!/usr/bin/env python3

''' Determine if two images have the same headers and footers. The headers and footers of an image
are extracted by taking a slice of the top and bottom of the image array. A structural similarity
index is taking to compare these slices between two images. The ssim has two outputs, a score
and a image that contain the differences. The score ranges [-1, 1] where 1 is a perfect match '''

import cv2
import shutil
import pprint
import numpy as np
import matplotlib as plt
import autopep8
import logging
import re
import glob

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

TEMP = '/tmp'
HEADER_SLICE_Y = 1000
HEADER_SLICE_X = 4500
BOUNDARY_THRESHOLD = 56.0
MATCH_THRESHOLD = 10


def read_image(image_path):
    ''' converts an image to a grayscale version of the images '''
    logger.info("reading image: {}".format(image_path))
    return cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2GRAY)


def preprocess_image(image):
    ''' preprocesses the image in order to improve the feature matching. It was
    shown that bilateral filtering does not give a good result. Image thresholding
    does improve the distinction between false and negatives '''
    # blur =  cv2.bilateralFilter(image,9,75,75)
    # blur =  cv2.medianBlur(image,3)
    return cv2.blur(image, (3, 3))
    # return image
    # blur = image
    # ret, imgf = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    # return imgf


def extract_header_image(image, header_y=HEADER_SLICE_Y, preprocess=True):
    ''' extracts the header an image. The image should be provided as a
    numpy array The header is indicated at the top 10% of an image '''
    if preprocess:
        image = preprocess_image(image)

    return image[0:HEADER_SLICE_Y]


def extract_header_path(image_path, header_y=HEADER_SLICE_Y, preprocess=True):
    ''' extracts the header of an image, image is firstly loaded from a given
    path. The header is indicated at the top 10% of an image '''
    if preprocess:
        image = preprocess(read_image(image_path))
    else:
        image = read_image(image_path)

    return image[0:HEADER_SLICE_Y]


def match_images(image1, image2, draw=False):
    ''' returns a list of sorted matches between two images. If the draw parameter
    is set to True, the keypoints will be shown on screen '''
    orb = cv2.ORB_create()
    kp1, des1 = orb.detectAndCompute(image1, None)
    kp2, des2 = orb.detectAndCompute(image2, None)
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    return matches


def similarity(image_path1, image_path2):
    ''' Returns true if the headers of two images have a good overlap '''
    header1 = extract_header(image_path1)
    header2 = extract_header(image_path2)
    return np.median([match.distance for match in match_images(header1, header2)])


def get_test_pairs(directory):
    ''' returns pairs of files. This is used to get testpairs in which each
    pair has similar headers (positive_pairs) or different headers (negative_pairs)'''
    test_images = sorted(glob.glob(directory + "*.png"))
    pairs = [(test_images[i], test_images[i + 1]) for i in range(0, len(test_images), 2)]
    return pairs


if __name__ == '__main__':
    # analyze pairs of images that have overlapping headers
    positive_d = '/home/t/pythoncode/headerdiff/data/testpairs/positive_pairs/'
    negative_d = '/home/t/pythoncode/headerdiff/data/testpairs/negative_pairs/'

    pos = get_test_pairs(positive_d)
    neg = get_test_pairs(negative_d)

    pos_distances = [similarity(pair[0], pair[1]) for pair in pos]
    neg_distances = [similarity(pair[0], pair[1]) for pair in neg]

    print(pos_distances)
    print(neg_distances)

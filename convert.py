#!/usr/bin/env python3

''' Convert pdf documents to a series of png image files'''
import os
import logging
import shutil
import subprocess
import glob


logger = logging.getLogger()

TEMP = '/tmp'


def create_temp_dir(filename):
    ''' creates a temp directory based on the filename given, deletes
    the directory if it already exists'''
    directory, fullname = os.path.split(filename)
    filename, ext = os.path.splitext(fullname)
    dump_path = os.path.join(TEMP, filename)
    try:
        shutil.rmtree(dump_path)
    except FileNotFoundError:
        logger.debug("dump directory {} not found, cannot be removed".format(dump_path))

    try:
        os.makedirs(dump_path)
        logger.info("directory {} created".format(dump_path))
    except FileExistsError:
        logger.info("directory already present, not creating again")

    return dump_path


def pdf_to_png(filename):
    ''' convert a pdf to a series of png images, these images will be stored
    in a tmp directory '''
    dump_dir = create_temp_dir(filename)
    directory, fullname = os.path.split(filename)
    new_location = os.path.join(dump_dir, fullname)
    shutil.copy(filename, new_location)
    logger.info("pdf file copied to {}".format(new_location))

    command = ['/usr/bin/pdftocairo', '-r', '600',
               '-png', new_location, dump_dir + '/']

    subprocess.run(command)

    return sorted(glob.glob(dump_dir + '/*.png'))


def pdf_to_png_range(filename, first, last, create=True):
    ''' convert the given pdf file to png images. The first and last page that should
    be converted are arguments. if create is set to False, there will be no new files
    created, use this for debugging when the files are already present'''
    dump_dir = create_temp_dir(filename)
    directory, fullname = os.path.split(filename)
    new_location = os.path.join(dump_dir, fullname)
    shutil.copy(filename, new_location)
    logger.info("pdf file copied to {}".format(new_location))
    logger.info("extracting png images: file={}, first={}, last={}".format(new_location, first, last))
    if create:
        command = ['/usr/bin/pdftocairo', '-r', '600', '-f',
                   str(first + 1), '-l', str(last + 1), '-png', new_location, dump_dir + '/']
        subprocess.run(command)

    return sorted(glob.glob(dump_dir + '/*.png'))

def make_combos(file_list):
    ''' gets the similarity score for all pairs in a list, for [1,2,3,4] this should give [(1,2), (2,3), (3,4)] '''
    combinations = []
    for n in range(len(file_list) - 1):
        combinations.append((file_list[n], file_list[n + 1]))
    return combinations

#!/usr/bin/env python3

''' Using opencv the keypoints between image pairs of a training set
were calculated. The training set consists of image pairs that have
overlap (homogram) or don't have overlapping parts. The goal is
to build a model that can be used to determine if two images are
similar or dissimilar. In order to determine the decision boundary
for overlapping and non overlapping images we have trained a simple
ML model. We train the model using the distances between overlapping
keypoints in a pair of images. In building this model it is assumed
that the (Hammond) distances between two matching keypoints of two images
can be used for training. Keypoints in two similar or identical images
will have many keypoints with a short distance between them. Images that
have no similarity will have keypoints with a high distance between them.
On of our hypothesis is that the distribution of the list of found distances
can be used as a feature for our model, therefore we calculate a nine number
summary of lists of distances and use these as features for our model. However
it appears that these nine numbers are not needed, the model with the best
prediction uses only the mean and the length of the list as features'''

import pickle
import numpy as np
import sklearn
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import accuracy_score, classification_report
from sklearn.decomposition import PCA
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC
import seaborn as sns
import pandas as pd
import autopep8


PERCENTILES = [10.0, 25.0, 50.0, 75.0, 90.0]


def nine_numbers(distances):
    ''' get the nine numbers of a numpy array. These are: mean, 10%-midsummary, min, max
    and the five percentiles '''
    distance_arr = np.array(distances)
    percentiles = [np.percentile(distance_arr, p) for p in PERCENTILES]
    midsummary = np.mean(np.take(percentiles, [0, -1]))
    mean = np.mean(distance_arr)
    return np.array([mean, len(distance_arr)])


if __name__ == '__main__':
    pos = pickle.load(open("distances_pos.pickle", 'rb'))
    neg = pickle.load(open("distances_neg.pickle", 'rb'))
    pos_nine = np.array([nine_numbers(distances) for distances in pos])
    neg_nine = np.array([nine_numbers(distances) for distances in neg])
    training_data = np.concatenate([pos_nine, neg_nine])
    labels = [1] * len(pos_nine) + [0] * len(neg_nine)

    x_train, x_test, y_train, y_test = train_test_split(training_data, labels, test_size = 0.25)
    model = GaussianNB()
    model.fit(x_train, y_train)

    y_model = model.predict(x_test)
    #score = accuracy_score(y_test, y_model)

    # Print some model statistics
    # theta_ are the means for each feature by class
    # sigma_ is the variance for each feature by class
    print(model.theta_)
    print(model.sigma_)

    # Visual exploration of the data by dimension reduction
    model = PCA(n_components=2)
    model.fit(training_data)
    X_2D = model.transform(training_data)
    pca_dataframe = pd.DataFrame(data={'PCA1': X_2D[:, 0], 'PCA2': X_2D[:, 1], 'Bool': labels})
    sns.lmplot("PCA1", "PCA2", data=pca_dataframe, hue='Bool', fit_reg=False)
    # plt.show()

    svm1 = SVC(kernel='linear')
    scores = cross_val_score(svm1,  training_data, labels, cv=10)
    print(np.mean(scores))
    score = accuracy_score(y_test, y_model)
    print(score)
    print(model.theta_)
    print(model.sigma_)

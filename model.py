#!/usr/bin/env python3

''' build a  model in order to predict if two images have a homogram (using feature extraction)'''

import header_compare
import sklearn
from sklearn.naive_bayes import GaussianNB
import numpy as np
from matplotlib import pyplot as plt


def prepare_dataset(path):
    pairs = header_compare.get_test_pairs(path)
    distances = [header_compare.similarity(pair[0], pair[1]) for pair in pairs]

    # reshape the array so its suited for usage in a training model
    return np.array(distances)


if __name__ == '__main__':
    # create the datapoints
    # analyze pairs of images that have overlapping headers
    positive_d = '/home/t/pythoncode/headerdiff/data/testpairs/positive_pairs/'
    negative_d = '/home/t/pythoncode/headerdiff/data/testpairs/negative_pairs/'

    pos = prepare_dataset(positive_d)
    neg = prepare_dataset(negative_d)
    data = np.append(pos, neg).reshape(-1, 1)
    labels = np.array([1] * len(pos) + [0] * len(neg))

    unknown_test = np.array([43, 66]).reshape(-1, 1)

    gnb = GaussianNB()
    gnb.fit(data, labels)
    predicted = gnb.predict(unknown_test)
    print(predicted)

    fig, axis = plt.subplots(1, 1)

    plotdata = (pos, neg)
    label = ("positive", "negative")
    color = ("red", "green")
    colors = [color[i] for i in labels]
    labels = [label[i] for i in labels]

    axis.scatter(len(data) * [1], data, c=colors)
    plt.savefig('t1.png')

    print(gnb.theta_)
    print(gnb.sigma_)

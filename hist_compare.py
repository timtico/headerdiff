#!/usr/bin/env python3

import cv2
from main import get_test_pairs

''' compare the histogram of two images '''


def read_color_image(image_path):
    return cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2GRAY)


def compare_histograms(image1, image2):
    ''' compare two images using their histograms '''
    image1 = read_color_image(image1)
    image2 = read_color_image(image2)

    h1 = cv2.calcHist([image1], [0], None, [256], [0, 256])
    h2 = cv2.calcHist([image2], [0], None, [256], [0, 256])

    # return cv2.compareHist(h1, h2, cv2.HISTCMP_CORREL)
    # return cv2.compareHist(h1, h2, cv2.HISTCMP_CHISQR)
    # return cv2.compareHist(h1, h2, cv2.HISTCMP_BHATTACHARYYA)
    # return cv2.compareHist(h1, h2, cv2.HISTCMP_INTERSECT)
    return cv2.compareHist(h1, h2, cv2.HISTCMP_HELLINGER)


if __name__ == '__main__':
    # analyze pairs of images that have overlapping headers
    positive_d = '/home/t/pythoncode/headerdiff/data/testpairs/positive_pairs/'
    negative_d = '/home/t/pythoncode/headerdiff/data/testpairs/negative_pairs/'

    pos = get_test_pairs(positive_d)
    neg = get_test_pairs(negative_d)

    hist_pos = [compare_histograms(p[0], p[1]) for p in pos]
    hist_neg = [compare_histograms(p[0], p[1]) for p in neg]
    print(hist_pos, hist_neg, sep="\n")

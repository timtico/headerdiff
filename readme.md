Document Header Similarity
---
A training set is generated containing manually selected images that were exported from corpus of PDF documents. There are two sets of images. The positives set consists of pairs of images that have a homogram, which means that the pairs have identical or near identical headers. Here we define
the header as the first n rows of pixels in the image array. By default n = 1000 pixels.
The negative set consists of image that have no homogram in the header. 

Keypoints in the image pairs are detected using the ORB algorithm. These keypoints are
then matched using a Brute Force matcher. The Hamming distance between each keypoint pair are 
collected and used to train a Support Vector Machine model. 

The model accuracy is 92%, which was determined by cross-validation (n=10).


# Dependencies

- opencv
- sklearn

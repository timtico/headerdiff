#!/usr/bin/env python3

''' Modelling a pair of images that '''
from header_compare import read_image, extract_header_image, match_images, HEADER_SLICE_Y
import convert
import create_dataset
import numpy as np
import os
import pickle


class ImagePair():
    ''' two images and their keypoint distances'''

    def __init__(self, first_image_path, second_image_path):
        self.first_image_path = first_image_path
        self.second_image_path = second_image_path
        self.first_image = read_image(self.first_image_path)
        self.second_image = read_image(self.second_image_path)
        self.get_matches()

    def __repr__(self):
        return "ImagePair(first_image_path = {!r}, second_image_path = {!r})".format(self.first_image_path, self.second_image_path)

    def __str__(self):
        return "{{first_image={!r}, second_image={!r}, len_matches = {!r}, matches_median={!r}}}".format(self.first_image_path, self.second_image_path, len(self.distances), self.distances_median)

    def get_matches(self, header_y=HEADER_SLICE_Y, pre_process=True):
        ''' takes a header slice of the images and calculates the overlapping
        keypoint between the images'''
        self.first_header = extract_header_image(self.first_image, header_y, pre_process)
        self.second_header = extract_header_image(self.second_image, header_y, pre_process)
        self.matches = match_images(self.first_header, self.second_header)
        self.distances = [d.distance for d in self.matches]
        self.distances_median = np.median(self.distances)


def create_training_set(file_with_index):
    ''' creates a training set. The training set is a list of ImagePairs instances
    with a list of distances as attribute '''
    images = []

    pairs = file_with_index

    for path, index in pairs.items():
        pdf_file_path = os.path.join(create_dataset.TESTDIR, path)
        for pair_index in index:
            file_paths = convert.pdf_to_png_range(pdf_file_path, pair_index[0], pair_index[1])
            images.append(ImagePair(file_paths[0], file_paths[1]))

    return images


if __name__ == '__main__':
    #images_pos = create_training_set(create_dataset.pos_pairs)
    #distances_pos = [ip.distances for ip in images_pos]
    #pickle.dump(distances_pos, open("distances_pos.pickle", 'wb'))

    images_neg = create_training_set(create_dataset.neg_pairs)
    distances_neg = [ip.distances for ip in images_neg]
    pickle.dump(distances_neg, open("distances_neg.pickle", 'wb'))

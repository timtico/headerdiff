#!/usr/bin/env python3

''' explore the distribution of the match objects by using
scatterplots and the big five '''

from model import prepare_dataset
from header_compare import match_images, extract_header, get_test_pairs
import pprint
from matplotlib import pyplot as plt

ORDER_SIZE = 100


def ordered_dataset(matches, size=ORDER_SIZE):
    return sorted(matches, key=lambda x: x.distance)[0:size]


def get_distances(image_path1, image_path2):
    header1 = extract_header(image_path1)
    header2 = extract_header(image_path2)
    return match_images(header1, header2)


if __name__ == '__main__':
    positive_d = '/home/t/pythoncode/headerdiff/data/testpairs/positive_pairs/'
    negative_d = '/home/t/pythoncode/headerdiff/data/testpairs/negative_pairs/'

    positive_pairs = get_test_pairs(positive_d)
    negative_pairs = get_test_pairs(negative_d)

    pos_matches = [get_distances(pair[0], pair[1]) for pair in positive_pairs]
    neg_matches = [get_distances(pair[0], pair[1]) for pair in negative_pairs]

    #pos_dist = ordered_dataset(pos_matches)
    #neg_dist = ordered_dataset(neg_matches)

    posd = [sorted([mo.distance for mo in l]) for l in pos_matches]
    negd = [sorted([mo.distance for mo in l]) for l in neg_matches]

    plt.scatter(posd[0], len(posd[0]) * [1], facecolors='none', edgecolors='b')
    plt.savefig('t2.png')
    # fig = plt.figure.Figure()
    # for i in range(1, len(posd) - 1):
    #     ax = fig.add_subplot(2, 3, i)
    #     ax.scatter(len(posd[i]) * [1], posd[i])
    #     print(posd[i])

    # plt.pyplot.savefig('t2.png')
    # # scatterplots for each list of
    # # pprint.pprint(posd)
    # # pprint.pprint(negd)
